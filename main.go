package main

import (
	"Go_Test1/printTest"
	"fmt"
)

func main() {
	//print
	printTest.TestPrint()
	testObj := printTest.TestObj{}
	fmt.Println(testObj)
	fmt.Println("---Start!!!---")
	// printTest.PrintTestLowercase()

	//float
	// float_test1()

	//slice(list)
	// sliceTest1()
	// sliceTest2()

	//array
	// ArrayTestPrint()

	//for
	// ForTest1()
	// ForTestArray()

	//if
	// ifTest1()
	// ifTest1()

	//switch
	// switchTest1()

	//func
	// funcTest1()
	// funcTest2()

	//defer
	// deferTest1()
	// deferTest2()
	// deferTest3()
	// defer_panic_test1()
	// defer_panic_test2()

	//delegate
	// delegateTest1()
	// lambdaTest1()

	//panic
	// panicTest1()

	//error
	// errorTest1()

	//string
	// stringTest1()

	//stringBuilder
	// stringBuilderTest1()

	//strconv
	// strconvTest1()

	//global
	// globalTest1()

	//map
	// mapTest1()

	//threadSafe
	// threadSafe.SyncMapTest1()
	// threadSafe.ConncurrentMapTest1()
	// threadSafe.MapNotThreadSafe()
	// threadSafe.SyncMapReadWriteDiffKey(10000)

	//struct
	// structTest1.StructTestValueType()
	// structTest2.StructTestRefType()

	//inherit
	// inheritTest1()

	//interface
	// interfaceTest1()

	//enum
	// enumTest1()

	//time
	time_test1()
}
